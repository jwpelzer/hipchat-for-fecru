# HipChat Fisheye Plugin

This is a simple Fisheye plugin that allows you to notify one or more HipChat rooms a commit is received in a repo.

## Configuration

To use this, you'll first need to register your [HipChat admin API token](https://hipchat.com/admin/api) into
the HipChat Configuration page in FishEye (**Administration > Security Settings > HipChat Configuration**). Navigate
to the repository you want to enable HipChat on (**Administration > Repository Settings > {Your Repository} > HipChat
Notification (in the action drop-down)**) then pick the rooms you want to notify.

## Fork and be happy

I'll likely be adding more integrations over time. If you'd like to contribute, feel free to fork and send me a pull request.