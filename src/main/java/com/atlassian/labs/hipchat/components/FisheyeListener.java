package com.atlassian.labs.hipchat.components;


import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fisheye.event.CommitEvent;
import com.atlassian.fisheye.spi.data.ChangesetDataFE;
import com.atlassian.fisheye.spi.services.RevisionDataService;
import com.atlassian.labs.hipchat.utils.HipChatProxyClient;
import com.atlassian.sal.api.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.StringTokenizer;

public class FisheyeListener implements DisposableBean
{

    private static final Logger log = LoggerFactory.getLogger(FisheyeListener.class);
    private final ConfigurationManager configurationManager;
    private final EventPublisher eventPublisher;
    private final ApplicationProperties applicationProperties;
    private final HipChatProxyClient hipChatProxyClient;
    private final RevisionDataService revisionDataService;

    public FisheyeListener(EventPublisher eventPublisher, ApplicationProperties applicationProperties,
                           ConfigurationManager configurationManager, RevisionDataService revisionDataService)
    {
        this.eventPublisher = eventPublisher;
        this.applicationProperties = applicationProperties;
        this.configurationManager = configurationManager;
        this.revisionDataService = revisionDataService;
        eventPublisher.register(this);
        this.hipChatProxyClient = new HipChatProxyClient(configurationManager);
    }

    @EventListener
    public void onCommit(CommitEvent event)
    {
        String repoName = event.getRepositoryName();

        ChangesetDataFE cs = revisionDataService.getChangeset(
                repoName, event.getChangeSetId());

        String url = applicationProperties.getBaseUrl() + "/changelog/" + repoName + "?cs=" + event.getChangeSetId();
        String changeSetId = event.getChangeSetId();
        String msg = "<b>" + cs.getAuthor() + "</b> committed to "
                + cs.getBranch() + " at "
                + "<a href=\"" + applicationProperties.getBaseUrl() + "/browse/" + repoName + "\">" + "/browse/"
                + repoName + "</a>" + " - " + cs.getComment() + "(<a href=\"" + url + "\">"
                + (changeSetId.length() > 12 ? changeSetId.substring(0, 12) : changeSetId) + "</a>)";

        String roomsToNotify = configurationManager.getHipChatRooms(event.getRepositoryName());
        StringTokenizer rooms = new StringTokenizer(roomsToNotify, ",");

        while (rooms.hasMoreTokens()) {
            hipChatProxyClient.notifyRoom(rooms.nextToken(), msg, applicationProperties.getBaseUrl());
        }
    }

    @Override
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }
}
