package com.atlassian.labs.hipchat.components;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class ConfigurationManager {
    // TODO for some reason FeCru throws up when createSettingsForKey is non null. So, I'm gonna disable this for
    // now and attach the auth-token to the global space
    // private static final String PLUGIN_STORAGE_KEY = "com.atlassian.labs.hipchat";
    private static final String PLUGIN_STORAGE_KEY = null;
    private static final String HIPCHAT_AUTH_TOKEN_KEY = "hipchat-auth-token";
    private static final String HIPCHAT_KEY_PREFIX = "hipchat-repo-";

    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfigurationManager(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getHipChatAuthToken() {
        return getValue(HIPCHAT_AUTH_TOKEN_KEY);
    }
    
    public String getHipChatRooms(String repoName){
        return getValue(HIPCHAT_KEY_PREFIX + repoName);
    }

    private String getValue(String storageKey) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        Object storedValue = settings.get(storageKey);
        return storedValue == null ? "" : storedValue.toString();
    }

    public void updateConfiguration(String authToken) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(HIPCHAT_AUTH_TOKEN_KEY, authToken);
    }

    public void setNotifyRooms(String repoName, String rooms) {
        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(PLUGIN_STORAGE_KEY);
        settings.put(HIPCHAT_KEY_PREFIX + repoName, rooms);
    }


}