package com.atlassian.labs.hipchat.utils;

import com.atlassian.labs.hipchat.components.ConfigurationManager;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HipChatProxyClient
{
    private static final Logger log = LoggerFactory.getLogger("atlassian.plugin");
    private static String API_BASE_URI = "https://api.hipchat.com";
    private static String MSG = " - new commit by ";
    private HttpClient httpClient;
    private ConfigurationManager configurationManager;

    public HipChatProxyClient(ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
        MultiThreadedHttpConnectionManager connectionManager =
                new MultiThreadedHttpConnectionManager();
        httpClient = new HttpClient(connectionManager);
    }

    public String getUser(String userId)
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (authToken.equals("")) return "";
        GetMethod get = new GetMethod(API_BASE_URI + "/v1/users/show?auth_token=" + authToken + "&user_id=" + userId);
        get.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

        try {
            int statusCode = httpClient.executeMethod(get);
            if (statusCode != HttpStatus.SC_OK) {
                log.info("Method failed: " + get.getStatusLine());
            }

            byte[] responseBody = get.getResponseBody();

            return new String(responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            get.releaseConnection();
        }

        return "";
    }

    private String getIconUrl(String baseUrl){
        return "<img src=\"" +
                baseUrl + "/images/icons/blogentry_16.gif" +
                "\" width=16 height=16 />&nbsp;";
    }

    public String getRooms() throws InvalidAuthTokenException
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (authToken.equals("")) return "";
        GetMethod get = new GetMethod(API_BASE_URI + "/v1/rooms/list?auth_token=" + authToken);
        get.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

        try {
            int statusCode = httpClient.executeMethod(get);
            if (statusCode != HttpStatus.SC_OK) {
                log.info("Method failed: " + get.getStatusLine());
                throw new InvalidAuthTokenException();
            }

            byte[] responseBody = get.getResponseBody();

            return new String(responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            get.releaseConnection();
        }

        return "";
    }

    public void notifyRoom(String room, String msg, String baseUrl)
    {
        String authToken = configurationManager.getHipChatAuthToken();
        if (authToken.equals("")) return;
        PostMethod post = new PostMethod(API_BASE_URI + "/v1/rooms/message?auth_token=" + authToken);
        post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
        post.addParameter("room_id", room);
        post.addParameter("from", "FishEye");
        post.addParameter("message", msg);
        post.addParameter("format", "json");

        try {
            int statusCode = httpClient.executeMethod(post);
            if (statusCode != HttpStatus.SC_OK) {
                log.info("Method failed: " + post.getStatusLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            post.releaseConnection();
        }
    }
}